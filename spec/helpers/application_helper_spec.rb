require "rails_helper"
RSpec.describe ApplicationHelper, type: :helper do
  describe "#title" do
    it "returns the base-title" do
      expect(full_title).to eq Constants::BASE_TITLE
    end

    it "return the base-title when argument is blank" do
      expect(full_title(title: " ")).to eq Constants::BASE_TITLE
    end

    it "returns the full-title" do
      expect(full_title(title: "products")).to eq "products - #{Constants::BASE_TITLE}"
    end
  end
end
