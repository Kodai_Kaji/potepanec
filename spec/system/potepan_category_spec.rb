require 'rails_helper'
RSpec.describe "category", type: :system do
  let!(:categories) { create(:taxonomy, name: "Categories") }
  let!(:brand) { create(:taxonomy, name: "Brand") }
  let!(:bags) { create(:taxon, name: "Bags", taxonomy: categories) }
  let!(:rubys) { create(:taxon, name: "Rubys", taxonomy: brand) }
  let!(:product_relation_bags) { create(:product, name: "ruby_bag") }
  let!(:product_relation_rubys) { create(:product, name: "ruby_mug") }
  let!(:image) { create(:image, viewable: product_relation_bags.master) }
  let!(:price) { create(:price, variant: product_relation_bags.master) }
  let!(:find_taxon_categories) { Spree::Taxon.find_by(name: "Categories") }
  let!(:find_taxon_brand) { Spree::Taxon.find_by(name: "Brand") }
  let!(:taxons_nest_categories) { bags.move_to_child_of(find_taxon_categories) }
  let!(:taxons_nest_brand) { rubys.move_to_child_of(find_taxon_brand) }
  let(:product_url) { "a[href='/potepan/products/#{product_relation_bags.id}']" }
  let!(:classfication_bags) do
    Spree::Classification.create(product_id: product_relation_bags.id, taxon_id: bags.id)
  end
  let!(:classification_rubys) do
    Spree::Classification.create(product_id: product_relation_rubys.id, taxon_id: rubys.id)
  end

  it "displays selected category products successfully" do
    visit potepan_category_path find_taxon_categories.id
    expect(page).to have_title full_title(title: find_taxon_categories.name)
    within '.pageHeader' do
      expect(page).to have_content find_taxon_categories.name, count: 2
    end
    within '.panel-info' do
      expect(page).to have_content categories.name, count: 1
      expect(page).to have_content brand.name, count: 1
      expect(page).to have_content bags.name, count: 1
      expect(page).to have_content rubys.name, count: 1
    end
    within "#count-#{bags.id}" do
      expect(page).to have_content bags.all_products.count.to_s, count: 1
    end
    within "#count-#{rubys.id}" do
      expect(page).to have_content rubys.all_products.count.to_s, count: 1
    end
    within '.panel-info' do
      click_link "Bags"
    end
    expect(page).to have_content product_relation_bags.name, count: 1
    expect(page).not_to have_content product_relation_rubys.name
    within "#product-info-#{product_relation_bags.id}" do
      expect(page).to have_content product_relation_bags.display_price
      expect(page).to have_selector "img"
      expect(page).to have_selector product_url, count: 1
    end
  end
end
