require 'rails_helper'
RSpec.describe Potepan::CategoriesController, type: :controller do
  render_views
  describe ':show' do
    let!(:categories) { create(:taxonomy, name: "Categories") }
    let!(:brand) { create(:taxonomy, name: "Brand") }
    let!(:bags) { create(:taxon, name: "Bags", taxonomy: categories) }
    let!(:rubys) { create(:taxon, name: "Rubys", taxonomy: brand) }
    let!(:product) { create(:product, name: "ruby_bag") }
    let!(:image) { create(:image, viewable: product.master) }
    let!(:price) { create(:price, variant: product.master) }
    let!(:classfication_bags) do
      Spree::Classification.create(product_id: product.id, taxon_id: bags.id)
    end

    context "send correct http-request" do
      before do
        get :show, params: { id: bags.id }
      end

      it "returns http status 200" do
        expect(response).to have_http_status "200"
      end

      it "returns appropriate assigns @taxon" do
        expect(assigns(:taxon)).to eq bags
      end

      it "returns appropriate assigns @products" do
        expect(assigns(:products)).to eq bags.all_products
      end

      it "returns appropriate assigns @taxonomy" do
        expect(assigns(:taxonomies)).to eq Spree::Taxonomy.all
      end

      it "returns template successfully" do
        expect(response).to render_template(:show)
      end

      it "render taxonomy-name category" do
        expect(response.body).to include categories.name
      end

      it "render taxonomy-name brand" do
        expect(response.body).to include brand.name
      end

      it "render taxon-name bags" do
        expect(response.body).to include bags.name
      end

      it "render taxon-name rubys" do
        expect(response.body).to include rubys.name
      end
    end

    context "send wrong http-request that has empty taxon-id" do
      it "returns ActiveRecord::RecordNotFound" do
        expect { get :show, params: { id: "" } }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
