module ProductDecorator
  def related_products
    Spree::Product.joins(:taxons).includes(
      master: [:default_price, :images]
    ).where.not(
      spree_products: { id: id }
    ).in_taxons(taxons.ids).distinct.limit(Constants::RELATED_PRODUCTS_SHOW_LIMIT)
  end

  Spree::Product.prepend self
end
