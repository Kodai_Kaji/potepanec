require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let(:taxon) { create(:taxon) }
  let(:other_taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:other_product) { create(:product, taxons: [other_taxon]) }
  let!(:prod1) { create(:product, taxons: [taxon]) }
  let!(:prod2) { create(:product, taxons: [taxon]) }
  let!(:prod3) { create(:product, taxons: [taxon]) }
  let!(:prod4) { create(:product, taxons: [taxon]) }
  let!(:prod5) { create(:product, taxons: [taxon]) }

  it "does not return receiver" do
    expect(product.related_products).not_to include product
  end

  it "does not return product that have other taxon" do
    expect(product.related_products).not_to include other_product
  end

  it "return maximum 4 related products" do
    expect(product.related_products.count).to eq 4
  end

  it "return related products successflly" do
    expect(product.related_products).to eq [prod1, prod2, prod3, prod4]
  end
end
