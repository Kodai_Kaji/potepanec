require 'rails_helper'
RSpec.describe Potepan::ProductsController, type: :controller do
  render_views
  describe "#show" do
    context "send correct http-request" do
      let(:taxon) { create(:taxon) }
      let(:product) { create(:product, name: "test-product", taxons: [taxon]) }

      before do
        get :show, params: { id: product.id }
      end

      it "returns http success 200" do
        expect(response).to have_http_status "200"
      end

      it "return appropriate assigns" do
        expect(assigns(:product)).to eq product
      end

      it "returns template successfully" do
        expect(response).to render_template(:show)
      end

      it "render product's price" do
        expect(response.body).to include product.display_price.to_s
      end

      it "render product's name" do
        expect(response.body).to include product.name
      end
    end

    context "send wrong http-request" do
      it "rerutns ActiveRecord::RecordNotFound" do
        expect { get :show, params: { id: "" } }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    describe "#show - related products function" do
      let(:taxon) { create(:taxon) }
      let(:product) { create(:product, taxons: [taxon]) }
      let!(:prod1) { create(:product, taxons: [taxon]) }
      let!(:prod2) { create(:product, taxons: [taxon]) }
      let!(:prod3) { create(:product, taxons: [taxon]) }
      let!(:prod4) { create(:product, taxons: [taxon]) }

      before do
        get :show, params: { id: product.id }
      end

      it "returns http success 200" do
        expect(response).to have_http_status "200"
      end

      it "returns template successfully" do
        expect(response).to render_template "show"
      end

      it "returns appropriate assigns @products" do
        expect(assigns(:related_products)).to eq [prod1, prod2, prod3, prod4]
      end
    end
  end
end
