require 'rails_helper'
RSpec.describe "products", type: :system do
  describe "detailed product" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, name: "test-product", taxons: [taxon]) }
    let!(:image) { create(:image, viewable: product.master) }

    it "display products-show page succsessfully" do
      visit potepan_product_path product.id
      expect(page).to have_title full_title(title: product.name)
      within "#product_name_left" do
        expect(page).to have_content product.name
      end
      within "#product_name_right" do
        expect(page).to have_content product.name
      end
      within "#product_name_center" do
        expect(page).to have_content product.name
      end
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
      within ".item" do
        expect(page).to have_selector("img")
      end
      within ".thumb" do
        expect(page).to have_selector("img")
      end
      expect(page).to have_link "一覧ページへ戻る", href: potepan_category_path(taxon.id)
      within "#top_logo" do
        expect(page).to have_link href: potepan_path
      end
      within "#link_home_up" do
        expect(page).to have_link "Home", href: potepan_path
      end
      within "#link_home_down" do
        expect(page).to have_link "Home", href: potepan_path
      end
    end
  end

  describe "related products" do
    let!(:products) { create_list(:product, 5, name: "test-products") }
    let(:taxon) { create(:taxon, name: "taxon") }
    let(:other_taxon) { create(:taxon, name: "other_taxon") }
    let(:show_product) { create(:product, name: "show_product", taxons: [taxon]) }
    let!(:product) { create(:product, name: "product", taxons: [taxon]) }
    let(:other_product) { create(:product, name: "other_product", taxons: [other_taxon]) }

    it "display related products successfully" do
      visit potepan_product_path show_product.id
      within ".productsContent" do
        expect(page).not_to have_content show_product.name
        expect(page).not_to have_content other_product.name
        expect(page).to have_content product.name
        expect(page).to have_link href: potepan_product_path(product.id)
        within "#product_box_#{product.id}" do
          expect(page).to have_content product.display_price
          expect(page).to have_selector "img"
        end
      end
    end
  end
end
